#!/bin/bash

last_version=`tail -1 .versions`

echo $last_version+0.01 | bc >> .versions

git add .

echo -n "\nDigite a mensagem para o commit : "

read commitMessage

git commit -m "${commitMessage}"

git push origin hotfix/v${last_version}

git flow hotfix finish v${last_version}

git checkout master
git push origin master

git checkout production
git push origin production
