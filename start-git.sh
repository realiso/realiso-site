#!/bin/bash

last_version=`tail -1 .versions`

git checkout master
git pull origin master

git checkout production
git pull origin production

git flow hotfix start v${last_version}

git checkout hotfix/v${last_version}

echo "##########################################################"
echo "# Edite os arquivos e depois rode o script ./end-git.sh. #"
echo "##########################################################"
