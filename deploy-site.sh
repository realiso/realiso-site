#! /bin/bash
#
# All bugs added by jonathan pirovano
# 

echo "=============================================================="
read -p "Selecione o ambiente para deploy do site (homolog,prod) [ENTER]:" ambiente;
if [ "$ambiente" == "prod" ]; then
  echo "Ambiente : PRODUÇÃO."
elif [ "$ambiente" == "homolog" ]; then
  echo "Ambiente : HOMOLOGAÇÃO."
else
  echo "Cancelado, ambiente não suportado."
  exit 1
fi

echo "Compactando arquivos para deploy."

nome='deploy_'$(date +"20%y_%m_%d") 

mkdir tmp_deploy
cp -r asaas tmp_deploy/
cp -r site tmp_deploy/

tar czf $nome.tar.gz tmp_deploy/

#
# Näo me pergunte porque diabos colocaram os dois sites
# homolog e producao no mesmo servidor.
# Caso mude, ajustar abaixo.
#
if [ "$ambiente" == "prod" ]; then
	
	echo "Efetuando deploy no ambiente de PRODUÇÃO."
	scp $nome.tar.gz ubuntu@www.realiso.com:/tmp/

elif [ "$ambiente" == "homolog" ]; then
  	
  	echo "Efetuando deploy no ambiente de HOMOLOGAÇÃO."
	scp $nome.tar.gz ubuntu@www.realiso.com:/tmp/

fi

echo "Excluindo arquivos temporários."
	
rm -rf tmp_deploy

echo " Deploy efetuado com sucesso. "
echo "=============================================================="
