<?php
include 'libs/Logger.php';
include 'libs/Email.php';
include_once 'config.php';

$client = new SoapClient(Config::instance()->asaas_accountbean);

$keyClientISMS = "realiso_yaklgghv76";
$registerISMS = array("keyClient" => $keyClientISMS);
$assinaturasISMS = $client->clientChangedStatusSubscription($registerISMS);
$numberOfUsers = 1;
$miLicenseType = 1;

$mail = new Email();
$mail->setSubject('ERROR: ASaaS n�o retornou os dados - RealISMS');

/* Syncronizing ISMS */
logger(SYNC_JOB, "Iniciando sincronismo ISMS");
if($assinaturasISMS->return){
	foreach ($assinaturasISMS->return as $assinatura){
		
		// Caso as assinaturas forem setadas para bloquear ou suspender ou terminar o trial, deve-se bloquear no saasadmin e setar o status no asaas.
		if($assinatura->status=="WAIT_END_TRIAL" or $assinatura->status=="WAIT_BLOCKED" or $assinatura->status=="WAIT_SUSPENDED"){
			logger(SYNC_JOB, "Bloquear >> $assinatura->status - $assinatura->company - $assinatura->accountId - $assinatura->expirationDate ");
			$instanceIsActive = 0;
			try{
				
				$status = file_get_contents(Config::instance()->url_saas_change_instance."?alias=".$assinatura->company."&active=".$instanceIsActive."&users=".$numberOfUsers."&license=".$miLicenseType); 
                
				if($status=="1"){
					logger(SYNC_JOB, "Instancia Bloqueada >> $assinatura->company ");
					$setNewStatusISMS = array("keyClient" =>$keyClientISMS, "keySubscription" => $assinatura->accountId);
			                $client->clientConfirmStatusSubscription($setNewStatusISMS);
        	    		} else {
					logger(SYNC_JOB, "Instancia NAO Bloqueada >> $assinatura->company");
				}
			} catch (Exception $e){
				logger(SYNC_JOB, "ERROR: Falha na modificacao do status no ASaaS" . var_export($client->clientFailStatusSubscription($registerISMS), true));
			}
		} else if($assinatura->status=="WAIT_TRIAL" or $assinatura->status=="WAIT_ACTIVED"){
			logger(SYNC_JOB, "Liberar >> $assinatura->status - $assinatura->company - $assinatura->accountId - $assinatura->expirationDate");
			$instanceIsActive = 1;
			try{
				
				$status = file_get_contents(Config::instance()->url_saas_change_instance."?alias=".$assinatura->company."&active=".$instanceIsActive."&users=".$numberOfUsers."&license=".$miLicenseType); 
                
                if($status=="1"){
					logger(SYNC_JOB, "Instancia Liberada >> $assinatura->company");
					$setNewStatusISMS = array("keyClient" =>$keyClientISMS, "keySubscription" => $assinatura->accountId);
					$client->clientConfirmStatusSubscription($setNewStatusISMS);
				} else {
					logger(SYNC_JOB, "Instancia NAO Liberada >> $assinatura->company");
				}
			} catch (Exception $e){
				logger(SYNC_JOB, "ERROR: \n" .	var_export($client->clientFailStatusSubscription($registerISMS), true));
			}
		} else {
			logger(SYNC_JOB, "Nenhuma status encontrado para alterar.");
		}
	}
} else {
	logger(SYNC_JOB, "ERROR: assinaturasISMS NAO disponiveis.");
	$message = "Problema ao acessar as assinaturas do ISMS, os dados n�o foram recuperados!"."\n"."Erro encontrado as ".date('Y-m-d h:i:s')."\n";
	$mail->setMessage($message);
        $mail->sendEmail();

}	

?>
