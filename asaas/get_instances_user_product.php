<?php

include_once 'config.php';

define("SUBSCRIPTION_ISMS", 1);
define("SUBSCRIPTION_SOX", 8);
define("SUBSCRIPTION_BCMS", 14);

define("LANG_PT_BR", 1);
define("LANG_EN", 2);

function getInstancesUserProduct($productId, $login, $lang){
/*
* Esta fun��o � chamada quando o usu�rio preenche os dados na tela de login
* para carregar as intancias do usu�rio no combobox da pagina.
*/
    
    $ip = $_SERVER["REMOTE_ADDR"];

    $productSelected = 0;

    $conn = mysql_connect(Config::instance()->mysql_host, Config::instance()->mysql_user, Config::instance()->mysql_pass ); 
    if (!$conn) {
        die(json_encode(array ("status" => "error", "error" => mysql_error())));
    }

    mysql_select_db(Config::instance()->mysql_db) or die (json_encode(array ("status" => "error", "error" => mysql_error())));

    switch($productId){
      case SUBSCRIPTION_ISMS:
        $productSelected = 5802;
        break;
      case SUBSCRIPTION_BCMS:
        $productSelected = 5816;
        break;
      case SUBSCRIPTION_SOX:
        $productSelected = 5808;
        break;
    }

    //mysql_query("DELETE FROM realiso_login WHERE start_time < DATE_SUB(NOW(), INTERVAL 30 MINUTE);");

    $getNumberAttempts = mysql_query("SELECT attempts FROM realiso_login WHERE ip='".$ip."';");
    $numberAttempts = mysql_fetch_row($getNumberAttempts);

    if (!$numberAttempts[0]){
        mysql_query("INSERT INTO realiso_login (ip, attempts) values ('" . $ip . "', 1);");
    } else if ($numberAttempts[0]==5){
        $getTimeBlocked = mysql_query("SELECT start_time FROM realiso_login WHERE ip='".$ip."';");
        $timeBlocked = mysql_fetch_row($getTimeBlocked);

        $now = time();
        $until = strtotime($timeBlocked[0]);

        if($now < $until){
            $diff = $until - $now;
            $minutos=date("i", $diff+59);

            if(setInternationalization($lang) == LANG_PT_BR) {
                $stringPT = "Atingiu o limite m�ximo, por favor volte" . $minutos . " em minutos!";
                echo json_encode(array ("status" => "error", "error" => utf8_encode($stringPT) ));
                return 0;
            } elseif (setInternationalization($lang) == LANG_EN) {
                $stringEN = "You have reached the maximum limit, please come back in " . $minutos . " minutes!";
                echo json_encode(array ("status" => "error", "error" => $stringEN ));
                return 0;
            }
        } else {
            mysql_query("DELETE from realiso_login WHERE ip='" . $ip . "';");
        }
    } else {
      $countAttempts = intval($numberAttempts[0]) + 1;
      mysql_query("UPDATE realiso_login SET attempts = " . $countAttempts . " WHERE ip='" . $ip . "';");
      if($numberAttempts[0]==4){
        mysql_query("UPDATE realiso_login set start_time = DATE_ADD(NOW(), INTERVAL 5 MINUTE) WHERE ip='" . $ip . "';");
      }
    }

    $getInstance = mysql_query("SELECT instance FROM realiso_users WHERE email='".$login."' AND fkproduct='".$productSelected."';");

    $instances[] = "";
    $cont=0;
    while($inst = mysql_fetch_row($getInstance)){
      $instances[$cont]=$inst[0];
      $cont++;
    }

    if(count($instances)==1){
        if($instances[0]){
            echo json_encode(array ("status" => "success", "inst" => $instances ));
            return 0;
        }else{
            if(setInternationalization($lang) == LANG_PT_BR) {
                echo json_encode(array ("status" => "error", "error" => utf8_encode('E-mail n�o encontrado!') ));
                return 0;
            } elseif (setInternationalization($lang) == LANG_EN) {
                echo json_encode(array ("status" => "error", "error" => 'E-mail not found!' ));
                return 0;
            }
        }
    }else{
        echo json_encode(array ("status" => "success", "inst" => $instances ));
        return 0;
    }

    clean_cache();
    mysql_close($conn);

}

function clean_cache(){
  if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
      $parts = explode('=', $cookie);
      $name = trim($parts[0]);
      setcookie($name, '', time()-1000);
      setcookie($name, '', time()-1000, '/');
    }
  }
}

function setInternationalization($lang){
    switch ($lang){
      case "pt-br":
          return LANG_PT_BR;
          break;
      case "pt":
          return LANG_PT_BR;
          break;
      case "en":
          return LANG_EN;
          break;
      default:
          return LANG_EN;
          break;
  }
}

$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$login = $_POST['data'][0]['name'];
$productId = $_POST['data'][1]['name'];

getInstancesUserProduct($productId, $login, $language);

?>
