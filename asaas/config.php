<?php

class Config {
 
  private static $instance;
  
  private $settings = array();
 
  public static function instance() {
    if (!isset(self::$instance)) {
      self::$instance = new self();
    }
    return self::$instance;
  }

  function Config() {
        
        $APP_ENV = 'production'; //teste Ajustar, buscar automaticamente,...
         
        if($APP_ENV=='dev'){

            //Desenvolvimento
            $this->settings['bcms_url_validacao_intancia']   = 'http://172.99.0.178/saasadmin/create_instance_validation.php';
            $this->settings['saas_isms_sox_url_home']        = 'https://homolog.realiso.com/';
            $this->settings['saas_bcms_url_home']            = 'http://homolog.realbcms.com/';
            $this->settings['isms_sox_url_create_instance']  = 'http://172.99.0.178/saasadmin/create_instance.php';
            $this->settings['bcms_url_create_instance']      = 'http://172.99.0.178/saasadmin/create_instance.php';
            $this->settings['asaas_accountbean']             = 'https://app.asaas.com/asaas-app-asaas-ejb-1.0-SNAPSHOT/AccountBean?wsdl';
            $this->settings['realiso_url_validacao_intancia']='http://172.99.0.178/saasadmin/create_instance_validation.php';  
            $this->settings['mysql_host'] = 'localhost:3306';  
            $this->settings['mysql_user'] = 'beta_realiso_com';  
            $this->settings['mysql_pass'] = '1nf0rm4nt';  
            $this->settings['mysql_db'] = 'beta_realiso_com';  
            $this->settings['log_register']     = "/home/real/Projects/realiso/realiso-site/asaas/logs/register.log";  
            $this->settings['log_sync_login']   = "/home/real/Projects/realiso/realiso-site/asaas/logs/sync_login.log";  
            $this->settings['log_sync_job']     = "/home/real/Projects/realiso/realiso-site/asaas/logs/sync_job.log";  
            $this->settings['url_saas_change_instance']     = "http://homolog.realiso.com/current/change_instance.php";  
            $this->settings['url_bcms_change_instance']     = "http://adminhomolog.realbcms.com/current/change_instance.php";  

        }else if($APP_ENV=='production'){

            //Production
            $this->settings['bcms_url_validacao_intancia']      = 'http://admin.realbcms.com/current/create_instance_validation.php';
            $this->settings['saas_isms_sox_url_home']           = 'https://saas.realiso.com/';
            $this->settings['saas_bcms_url_home']               = 'http://saas.realbcms.com/';
            $this->settings['isms_sox_url_create_instance']     = 'http://admin.realiso.com/current/create_instance.php.old';
            $this->settings['bcms_url_create_instance']         = 'http://admin.realbcms.com/current/create_instance.php.old';
            $this->settings['asaas_accountbean']                = 'https://app.asaas.com/asaas-app-asaas-ejb-1.0-SNAPSHOT/AccountBean?wsdl';
            $this->settings['realiso_url_validacao_intancia']   ='http://admin.realiso.com/current/create_instance_validation.php'; 
            $this->settings['mysql_host'] = 'localhost:3306';  
            $this->settings['mysql_user'] = 'realiso_com';  
            $this->settings['mysql_pass'] = '1nf0rm4nt';  
            $this->settings['mysql_db'] = 'realiso_com';  
            $this->settings['log_register']     = "/storage/www/www.realiso.com/realiso-site/asaas/logs/register.log";
            $this->settings['log_sync_login']   = "/storage/www/www.realiso.com/realiso-site/asaas/logs/sync_login.log";  
            $this->settings['log_sync_job']     = "/storage/www/www.realiso.com/realiso-site/asaas/logs/sync_job.log";
            $this->settings['url_saas_change_instance']     = "http://admin.realiso.com/current/change_instance.php";  
            $this->settings['url_bcms_change_instance']     = "http://admin.realbcms.com/current/change_instance.php";  
            
        }else{
            
            //Homolog
            $this->settings['bcms_url_validacao_intancia']      = 'http://adminhomolog.realbcms.com/current/create_instance_validation.php';
            $this->settings['saas_isms_sox_url_home']           = 'https://homolog.realiso.com/';
            $this->settings['saas_bcms_url_home']               = 'http://homolog.realbcms.com/';
            $this->settings['isms_sox_url_create_instance']     = 'http://homolog.realiso.com/current/create_instance.php';
            $this->settings['bcms_url_create_instance']         = 'http://adminhomolog.realbcms.com/current/create_instance.php';
            $this->settings['asaas_accountbean']                = 'https://app.asaas.com/asaas-app-asaas-ejb-1.0-SNAPSHOT/AccountBean?wsdl';
            $this->settings['realiso_url_validacao_intancia']   ='http://homolog.realiso.com/current/create_instance_validation.php'; 
            $this->settings['mysql_host'] = 'localhost:3306';  
            $this->settings['mysql_user'] = 'beta_realiso_com';  
            $this->settings['mysql_pass'] = '1nf0rm4nt';  
            $this->settings['mysql_db'] = 'beta_realiso_com';  
            $this->settings['log_register']     = "/storage/www/beta.realiso.com/asaas/logs/register.log";  
            $this->settings['log_sync_login']   = "/storage/www/beta.realiso.com/asaas/logs/sync_login.log";  
            $this->settings['log_sync_job']     = "/storage/www/beta.realiso.com/asaas/logs/sync_job.log";  
            $this->settings['url_saas_change_instance']     = "http://homolog.realiso.com/current/change_instance.php";  
            $this->settings['url_bcms_change_instance']     = "http://adminhomolog.realbcms.com/current/change_instance.php";  
            
        }

  }

  public function __get($name) {
    if (!isset($this->settings[$name]))
       throw new Exception('Config - propriedade não existe:'.$name);
    return $this->settings[$name];
  }
 
  public function __set($name, $value) {
    $this->settings[$name] = $value;
  }
}

?>