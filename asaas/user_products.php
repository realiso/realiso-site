<?php

include_once 'config.php';

define("SUBSCRIPTION_ISMS", 1);
define("SUBSCRIPTION_SOX", 8);
define("SUBSCRIPTION_BCMS", 14);

define("LANG_PT_BR", 1);
define("LANG_EN", 2);

function getURL($productId, $login, $pass, $lang, $instance){

    $ip = $_SERVER["REMOTE_ADDR"];

    $productSelected = 0;

    $conn = mysql_connect(Config::instance()->mysql_host, Config::instance()->mysql_user, Config::instance()->mysql_pass); 
    
    if (!$conn) {
        die(json_encode(array ("status" => "error", "error" => mysql_error())));
    }

    mysql_select_db(Config::instance()->mysql_db) or die (json_encode(array ("status" => "error", "error" => mysql_error()))); 
    
    $private_key = 'WawLQKv8EFCqUSTmT2vtTwFdUVsrWrIDabA6mbfTTbJ2zSxfQ9';

    switch($productId){
      case SUBSCRIPTION_ISMS:
        $productSelected = 5802;
        break;
      case SUBSCRIPTION_BCMS:
        $productSelected = 5816;
        break;
      case SUBSCRIPTION_SOX:
        $productSelected = 5808;
        break;
    }

    if($productId==SUBSCRIPTION_ISMS || $productId==SUBSCRIPTION_SOX){
    
      $productURL = Config::instance()->saas_isms_sox_url_home.$instance.'/sys/autologin.php'; 
    
    }else{

      $productURL = Config::instance()->saas_bcms_url_home.$instance.'/sys/autologin.php'; 
      
    }

    $data[0] = $login;
    $data[1] = $pass;
    $data[2] = time();

    $dataEncoded = json_encode($data, true);

    $dataEncrypt = encrypt($dataEncoded, $private_key);

    $url = $productURL . "?data=" . $dataEncrypt;

    mysql_query("DELETE from realiso_login WHERE ip='" . $ip . "';");

    //clean_cache();
    mysql_close($conn);

    echo json_encode(array ("status" => "success", "url" => $url));
}

function clean_cache(){
  if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
      $parts = explode('=', $cookie);
      $name = trim($parts[0]);
      setcookie($name, '', time()-1000);
      setcookie($name, '', time()-1000, '/');
    }
  }
}

function encrypt($string, $key){
  $result = '';
  for($i=0; $i<strlen($string); $i++){
    $char = substr($string, $i, 1);
    $keychar = substr($key, ($i % strlen($key))-1, 1);
    $char = chr(ord($char)+ord($keychar));
    $result.=$char;
  }
  return urlencode(base64_encode($result));
}

function setInternationalization($lang){
    switch ($lang){
      case "pt-br":
          return LANG_PT_BR;
          break;
      case "pt":
          return LANG_PT_BR;
          break;
      case "en":
          return LANG_EN;
          break;
      default:
          return LANG_EN;
          break;
  }
}

$language  = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$productId = $_POST['productId'];
$login     = $_POST['email'];
$pass      = $_POST['password'];
$instance  = $_POST['emailInstances'];

getURL($productId, $login, $pass, $language, $instance);

?>
