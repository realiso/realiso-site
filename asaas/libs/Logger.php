<?php

include_once 'config.php';

define("REGISTER"  , "1");
define("SYNC_LOGIN", "2");
define("SYNC_JOB"  , "3");

function logger($out, $content){
	$date = date('Y-m-d H:i:s');

	switch($out){
      case REGISTER:
        $log_destino = Config::instance()->log_register ;
        break;
      case SYNC_LOGIN:
        $log_destino = Config::instance()->log_sync_login;
        break;
      case SYNC_JOB:
        $log_destino = Config::instance()->log_sync_job;
        break;
    }

	file_put_contents($log_destino, $date . " - " . $content . "\n", FILE_APPEND);
}

?>
