<?php
include_once 'sync_login.php';
include_once 'config.php';

define("SUBSCRIPTION_ISMS", 1);
define("SUBSCRIPTION_SOX", 8);
define("SUBSCRIPTION_BCMS", 14);

define("VALIDATION_OK", 1);
define("ALIAS_ALREADY_IN_USE", 8);

define("CREATE_ASAAS_OK", 1);
define("CREATE_ASAAS_NOT_OK", 0);

define("CREATE_SAAS_ADMIN_OK", 1);
define("LICENSE_TYPE_SUBSCRIPTION_ISMS", 5802);
define("LICENSE_TYPE_SUBSCRIPTION_SOX", 5808);
define("LICENSE_TYPE_SUBSCRIPTION_BCMS", 5816);

define("LANG_PT_BR", 1);
define("LANG_EN", 2);

function setInstance($productId){
	switch($productId){
		case 1:
			return LICENSE_TYPE_SUBSCRIPTION_ISMS;
		case 8:
			return LICENSE_TYPE_SUBSCRIPTION_SOX;
		case 14:
			return LICENSE_TYPE_SUBSCRIPTION_BCMS;
	}
}

function createAccount($client, $registro, $company, $email, $pass, $productId, $lang){

    logger(REGISTER, "iniciando processo de criacao de conta...");
    logger(REGISTER, "validando instancia $company, $productId");

    $instance_validation = validadeInstanceSAASAdmin($company, $productId);

    if($instance_validation == VALIDATION_OK){
        logger(REGISTER, "criando conta no asaas " . var_export($registro, true));
        $client_id = createAccountAsaas($client, $registro, $lang);
        if($client_id){
            logger(REGISTER, "criando conta no saasadmin [company=$company , email=$email, pass=$pass , product=$productId , client=$client_id] ");
            $status = createInstanceSAASAdmin($company, $email, $pass, $productId, $client_id);
            #status
            if($status == CREATE_SAAS_ADMIN_OK){
                $fkproduct = setInstance($productId);
                logger(REGISTER, "inserindo login do usuario da instancia... $company, $email, $email, $fkproduct");

                insertLogin('13', $company, $email, $email, $fkproduct);

                logger(REGISTER, "sucesso! redirecionando usuario... $company, $productId");
                redirectToSAAS($productId, $company);
            } else {
                getErrorMessage($status, $company);
            }
        } else {
            logger(REGISTER, "erro no processo de criacao da instancia! $company");

            /*
            $email = new Email();
            $email->setSubject("Erro SaaSAdmin");
            $email->setMessage("Erro no processo de criacao da instancia! $company ($email)");
            $email->sendEmail();
  		    */

            if(setInternationalization($lang) == LANG_PT_BR){
                echo json_encode(array ("status" => "error", "error" => "Erro na criação da conta. Por favor, tente novamente!" ));
            }elseif (setInternationalization($lang) == LANG_EN) {
                echo json_encode(array ("status" => "error", "error" => "Error creating account. Please try again!" ));
            }
       }
    }
    elseif($instance_validation==ALIAS_ALREADY_IN_USE){
        if(setInternationalization($lang) == LANG_PT_BR){
            echo json_encode(array ("status" => "error", "error" =>  "Empresa já cadastrada! "));
        }
        elseif(setInternationalization($lang) == LANG_EN){
            echo json_encode(array ("status" => "error", "error" =>  "Company already registered! "));
        }
    }
}

function validadeInstanceSAASAdmin($company, $productId){
    
    if($productId==SUBSCRIPTION_BCMS){
        $validation = file_get_contents( Config::instance()->bcms_url_validacao_intancia."?alias={$company}");
    }else{
        $validation = file_get_contents( Config::instance()->realiso_url_validacao_intancia."?alias={$company}"); 
    }
        
    if($validation == 1){
        return VALIDATION_OK;
    }
    else{
        return ALIAS_ALREADY_IN_USE;
    }

}

function createAccountAsaas($client, $registro, $lang){
    
    $client_id = $client->clientNewRegisteredSubscription($registro);
    if (!$client_id->return->statusResponse){

        if(preg_match("/email informado/i", $client_id->return->exceptionResponse)){
            if(setInternationalization($lang) == LANG_PT_BR){
                echo json_encode(array ("status" => "error", "error" => $client_id->return->exceptionResponse ));
            } elseif(setInternationalization($lang) == LANG_EN) {
                echo json_encode(array ("status" => "error", "error" => 'You already have an active account to the informed email.' ));
            }
        } else {
            echo json_encode(array ("status" => "error", "error" => $client_id->return->exceptionResponse ));
        }

    	logger(REGISTER, "erro na criacao da conta asaas: " . $client_id->return->exceptionResponse);

    	exit();
    }else
        return $client_id->return->accountId;

}

function createInstanceSAASAdmin($company, $email, $pass, $productId, $client_id){
    $status = '';
    
    if($productId==SUBSCRIPTION_BCMS){
        $status = file_get_contents(Config::instance()->bcms_url_create_instance."?alias={$company}&email={$email}&pass={$pass}&license={$productId}&client_id={$client_id}");
    }else{
        $status = file_get_contents(Config::instance()->isms_sox_url_create_instance."?alias={$company}&email={$email}&pass={$pass}&license={$productId}&client_id={$client_id}");
    }
    
    return intval($status);
}

function redirectToSAAS($productId, $company){
	if($productId==SUBSCRIPTION_ISMS || $productId==SUBSCRIPTION_SOX){
		
    $url_instance = Config::instance()->saas_isms_sox_url_home.$company;
		echo json_encode(array ("status" => "success", "url" => $url_instance));
	
  }
	if($productId==SUBSCRIPTION_BCMS){
	
  	$url_instance = Config::instance()->saas_bcms_url_home.$company;
		echo json_encode(array ("status" => "success", "url" => $url_instance));
	
  }
}

function getErrorMessage($status, $company){
    echo json_encode(array ("status" => "error", "error" =>  "Erro na criação da sua conta, entre em contato com a RealISO."));
    $email = new Email();
    $email->setSubject("Erro no SAASAdmin");
    $email->setMessage("Erro na criaçao de conta no SAASAdmin. Cliente: $company");
    $email->sendEmail();
    
    // TODO List: Logar os erros no log do PHP, e não deve ser mostrado para o cliente!
    if($status == 2)
      logger(REGISTER, "Erro na criação da base de dados ");
    elseif($status == 3)
      logger(REGISTER, "Erro na execução dos scripts ");
    elseif($status == 4)
      logger(REGISTER, "Erro na copia dos arquivos PHPs ");
    elseif($status == 5)
      logger(REGISTER, "Erro no ajuste da base de dados");
    elseif($status == 6)
     logger(REGISTER, "Erro na inclusão da instância");
    elseif($status == 7)
     logger(REGISTER, "Licença Inválida ");
    else
     logger(REGISTER, "Erro desconhecido!");
}

function setInternationalization($lang){
    switch ($lang){
      case "pt-br":
          return LANG_PT_BR;
          break;
      case "pt":
          return LANG_PT_BR;
          break;
      case "en":
          return LANG_EN;
          break;
      default:
          return LANG_EN;
          break;
  }
}

$name = $_POST['name'];
$company = $_POST['company'];
$phone = $_POST['phone'];
$phone_country = $_POST['phone-country'];
$email = $_POST['email'];
$pass = $_POST['password'];
$planId = $_POST['planId'];
$productId = $_POST['productId'];

$complete_phone = "(".$phone_country.") " . $phone;

logger(REGISTER, "Iniciar conexão com o ASaaS...");

try{

   $client = new SoapClient(Config::instance()->asaas_accountbean);
   $keyClient = "";

}catch(Exception $ex){
  logger(REGISTER,"Falha na conexão SOAP:".$ex);
  exit();
}

logger(REGISTER, "Conexão com o ASaaS OK...");

switch($productId){
  case SUBSCRIPTION_ISMS:
  $keyClient = "realiso_yaklgghv76";
  break;
  case SUBSCRIPTION_BCMS:
  $keyClient = "realiso_qra5bxuwyo";
  break;
  case SUBSCRIPTION_SOX:
  $keyClient = "realiso_gxln0vb5dq";
  break;
}

$registro = array(
  "keyClient" => $keyClient,

  "subscription" => array("name" => $name,
                          "email" => $email,
                          "planCode" => $planId,
                          "company" => $company,
                          "phone" => $complete_phone,
                          "password" => $pass)
                    );

$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

createAccount($client, $registro, $company, $email, $pass, $productId, $language);
?>
