<?php
include 'libs/Logger.php';
include 'libs/Email.php';
include_once 'config.php';

$client = new SoapClient( Config::instance()->asaas_accountbean );

$keyClientISMS = "realiso_yaklgghv76";
$keyClientBCMS = "realiso_qra5bxuwyo";
$keyClientSOX  = "realiso_gxln0vb5dq";

$registerISMS = array("keyClient" => $keyClientISMS);
$registerBCMS = array("keyClient" => $keyClientBCMS);
$registerSOX  = array("keyClient" => $keyClientSOX);

$assinaturasISMS = $client->clientChangedStatusSubscription($registerISMS);
$assinaturasBCMS = $client->clientChangedStatusSubscription($registerBCMS);
$assinaturasSOX  = $client->clientChangedStatusSubscription($registerSOX);

$mail = new Email();
$mail->setSubject('ERROR: ASaaS n�o retornou os dados - RealISMS');

/* Syncronizing ISMS */
logger(SYNC_JOB, "Iniciando sincronismo ISMS");
if($assinaturasISMS->return){
	foreach ($assinaturasISMS->return as $assinatura){
		// Caso as assinaturas forem setadas para bloquear ou suspender ou terminar o trial, deve-se bloquear no saasadmin e setar o status no asaas.
		if($assinatura->status=="WAIT_END_TRIAL" or $assinatura->status=="WAIT_BLOCKED" or $assinatura->status=="WAIT_SUSPENDED"){
			logger(SYNC_JOB, "Bloquear >> $assinatura->status - $assinatura->company - $assinatura->accountId - $assinatura->expirationDate ");
			$instanceIsActive = 0;
			try{
			        $status = file_get_contents( Config::instance()->url_saas_change_instance."?alias=".$assinatura->company."&active=".$instanceIsActive);
                                
                                if($status=="1"){
					logger(SYNC_JOB, "Instancia Bloqueada >> $assinatura->company ");
					$setNewStatusISMS = array("keyClient" =>$keyClientISMS, "keySubscription" => $assinatura->accountId);
			                $client->clientConfirmStatusSubscription($setNewStatusISMS);
        	    		} else {
					logger(SYNC_JOB, "Instancia NAO Bloqueada >> $assinatura->company");
				}
			} catch (Exception $e){
				logger(SYNC_JOB, "ERROR: Falha na modificacao do status no ASaaS" . var_export($client->clientFailStatusSubscription($registerISMS), true));
			}
		} else if($assinatura->status=="WAIT_TRIAL" or $assinatura->status=="WAIT_ACTIVED"){
			logger(SYNC_JOB, "Liberar >> $assinatura->status - $assinatura->company - $assinatura->accountId - $assinatura->expirationDate");
			$instanceIsActive = 1;
			try{
				$status = file_get_contents(Config::instance()->url_saas_change_instance."?alias=".$assinatura->company."&active=".$instanceIsActive);
                                
                                if($status=="1"){
					logger(SYNC_JOB, "Instancia Liberada >> $assinatura->company");
					$setNewStatusISMS = array("keyClient" =>$keyClientISMS, "keySubscription" => $assinatura->accountId);
					$client->clientConfirmStatusSubscription($setNewStatusISMS);
				} else {
					logger(SYNC_JOB, "Instancia NAO Liberada >> $assinatura->company");
				}
			} catch (Exception $e){
				logger(SYNC_JOB, "ERROR: \n" .	var_export($client->clientFailStatusSubscription($registerISMS), true));
			}
		} else {
			logger(SYNC_JOB, "Nenhuma status encontrado para alterar.");
		}
	}
} else {
	logger(SYNC_JOB, "ERROR: assinaturasISMS NAO disponiveis.");
	$message = "Problema ao acessar as assinaturas do ISMS, os dados n�o foram recuperados!"."\n"."Erro encontrado as ".date('Y-m-d h:i:s')."\n";
	$mail->setMessage($message);
        $mail->sendEmail();

}	

/* Syncronizing SOX */
echo "=========================================\n";
echo "======= Iniciando sincronismo SOX =======\n";
echo "=========================================\n\n\n";
if ($assinaturasSOX->return){
foreach ($assinaturasSOX->return as $assinatura){
        // Caso as assinaturas forem setadas para bloquear ou suspender ou terminar o trial, deve-se bloquear no saasadmin e setar o status no asaas.
        if($assinatura->status=="WAIT_END_TRIAL" or $assinatura->status=="WAIT_BLOCKED" or $assinatura->status=="WAIT_SUSPENDED"){
                echo "Bloquear >> ".$assinatura->status." -- ".$assinatura->company." -- ".$assinatura->accountId." -- ".$assinatura->expirationDate."\n";
                $instanceIsActive = 0;
                try{
                        $status = file_get_contents(Config::instance()->url_saas_change_instance."?alias=".$assinatura->company."&active=".$instanceIsActive); 
                        
                        if($status=="1"){
                                echo "Instancia Bloqueada >> ". $assinatura->company ."\n";
                                $setNewStatusSOX = array("keyClient" =>$keyClientSOX, "keySubscription" => $assinatura->accountId);
                                $client->clientConfirmStatusSubscription($setNewStatusSOX);
                        } else {
                                echo "Instancia NAO Bloqueada >> ". $assinatura->company ."\n";
                        }
                } catch (Exception $e){
                        echo "ERROR\n";
                        var_dump($client->clientFailStatusSubscription($registerSOX));
                }
        } else if($assinatura->status=="WAIT_TRIAL" or $assinatura->status=="WAIT_ACTIVED"){
                echo "Liberar >> ".$assinatura->status." -- ".$assinatura->company." -- ".$assinatura->accountId." -- ".$assinatura->expirationDate."\n";
                $instanceIsActive = 1;
                try{
                        $status = file_get_contents(Config::instance()->url_saas_change_instance."?alias=".$assinatura->company."&active=".$instanceIsActive);
                        
                        if($status=="1"){
                                echo "Instancia Liberada >> ". $assinatura->company ."\n";
                                $setNewStatusSOX = array("keyClient" =>$keyClientSOX, "keySubscription" => $assinatura->accountId);
                                $client->clientConfirmStatusSubscription($setNewStatusSOX);
                        } else {
                                echo "Instancia NAO Liberada >> ". $assinatura->company ."\n";
                        }
                } catch (Exception $e){
                        echo "ERROR\n";
                        var_dump($client->clientFailStatusSubscription($registerSOX));
                }
        } else {
                echo "\nNenhuma status encontrado para alterar as ".date('Y-m-d h:i:s')."\n";
        }
}
} else {
	echo "\nERROR: assinaturasSOX NAO disponiveis, enviarndo email para suporte ...\n\n";
	$message = "Problema ao acessar as assinaturas do SOX, os dados n�o foram recuperados!"."\n"."Erro encontrado as ".date('Y-m-d h:i:s')."\n";
	$mail->setMessage($message);
        $mail->sendEmail();
}

?>
