var IMPLEMENT = '1';
var CONSULTANT = '2';
var CERTIFIED = '3';
var isAnimating = false;

function previousPhrase(button) {
    var shown = getPhraseShown(button);
    var phrase = shown.prev('p');
    animatePhrases(shown, phrase);

    enableNavButton(button.next());
    if (!phrase.prev().is("p")) disableNavButton(button);
}

function nextPhrase(button) {
    var shown = getPhraseShown(button);
    var phrase = shown.next('p');
    animatePhrases(shown, phrase);

    enableNavButton(button.prev());
    if (!phrase.next().is("p")) disableNavButton(button);
}

function getPhraseShown(button) {
    var parent = button.parent();
    return parent.find('p:visible');
}

function animatePhrases(shown, phrase) {
    if (isAnimating) return;
    isAnimating = true;
    shown.fadeOut('fast', function() {
        phrase.fadeIn('slow');
        isAnimating = false;
    });
}

function disableNavButton(button) {
    button.unbind('click');
    button.addClass('disabled');
    button.attr('title', '');
}

function enableNavButton(button) {
    button.unbind('click');
    button.removeClass('disabled');

    if (button.hasClass('prev')) {
        button.attr('title', 'Anterior');
        button.bind('click', function() { previousPhrase(button); });
        return;
    }

    button.attr('title', 'Próximo');
    button.bind('click', function() { nextPhrase(button); });
}

function includePages() {
    bindButtonEvents();
    bindCurtainBehavior();

    if (getCookie("realiso_curtain") == "hidden") {
        hideHeader();
    } else {
        jQuery("#header").css({"height" : "528px"});
    }
}

function bindButtonEvents() {
    //jQuery(".implement-button").click(implementButtonClick);
    //jQuery(".consultant-button").click(consultantButtonClick);
    //jQuery(".certified-company").click(certifiedButtonClick);
    jQuery("#why-implement").click(whyImplementButtonClick);
    jQuery(".show-header-link").click(showHeader);

    $(function(){
        jQuery("#slides-area").slides({
            generateNextPrev: false,
            play: 10000,
            slideSpeed: 500
        });
    });
}

function isHeaderVisible() {
    return (jQuery("#header-bottom").css("position") != "fixed")
}


function hideHeader(scrollPosition) {

    if (!isHeaderVisible()) {
        return;
    }

    jQuery("#header").animate(
        {
            height: "0px"
        },

        function() {
            jQuery(this).hide();
            jQuery("#header-bottom").css({"position": "fixed", "top" : "0px"});
            jQuery("#scrolling-area").css({"top": "53px"});
            jQuery(window).scrollTop(scrollPosition);
        }
    );

    setCookie("realiso_curtain", "hidden");
}


function showHeader() {
    jQuery(window).scrollTop(0);
    jQuery("#header-bottom").css({"position": "relative"});
    jQuery("#scrolling-area").css({"top": "0px"});
    jQuery("#header").show();
    jQuery("#header").animate(
        {
            height: "528px",
            top: "0px"
        });

    setCookie("realiso_curtain", "hidden");
}


function loadProductsHeader(option) {
    var element = jQuery("#header");

    element.load("html/header/products.html", function() {
        jQuery("#selected-option").val(option);
        if (option == IMPLEMENT) {
            jQuery("#header-breadcrumb-option").html("I WANT TO IMPLEMENT");
        } else if (option == CONSULTANT) {
            jQuery("#header-breadcrumb-option").html("I AM ISO CONSULTANT");
        } else if (option == CERTIFIED) {
            jQuery("#header-breadcrumb-option").html("MY COMPANY IS ALREADY ISO CERTIFIED");
        }

        curtain = option;
    });
}

function resetHeader() {
    jQuery("#header").load("html/header/header.html");
}


function setCookie(name, value) {
    document.cookie = name + "=" + escape(value) + "; path=/";
}


function getCookie (c_name) {
    if (document.cookie.length > 0) {
        var c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1)
                c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return null;
}

function bindCurtainBehavior() {
    jQuery(window).scroll(function() {
        var el = document.getElementById("header-bottom");
        var _x = 0;
        var _y = 0;
        while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.parentNode;
        }

        if (isHeaderVisible()) {
            if (_y <= 0) {
                jQuery("#header-bottom").css({"position": "fixed", "top" : "0px"});
                jQuery("#scrolling-area").css({"top": "53px"});
                jQuery("#header").css({"height":"0px"});
                jQuery("#header").hide(function(){});
            }
        }
    });
}


function implementButtonClick() {
	alert("Implementar!");
}


function consultantButtonClick() {
	alert("Consultor!");
}


function certifiedButtonClick() {
	alert("Certificado!");
}


function whyImplementButtonClick() {
	alert("Por que implementar?");
}

function getModalLeftPosition(modal) {
    return (jQuery(window).width() / 2) - (jQuery(modal).width() / 2);
}

function getModalTopPosition(modal) {
    return (jQuery(window).height() / 2) - (jQuery(modal).height() / 2);
}


function showOverlay() {
    var screenHeight = jQuery(document).height();
    var screenWidth = jQuery(window).width();
    var dialog = jQuery('#dialog-overlay');
    dialog.css({'width':screenWidth, 'height':screenHeight});
    dialog.fadeIn(800);
}

function showDialog(dialogId) {
    var dialog = jQuery(dialogId);
    dialog.css({'top': getModalTopPosition(dialog),'left': getModalLeftPosition(dialog)});
    dialog.show();
}

function showTrialDialog(product) {
    jQuery("#try-now-dialog").load("html/trial_dialog.html", function() {
        showOverlay();
        showDialog("#try-now-dialog");

        jQuery("#product").val(product);

        jQuery("#dialog-overlay").click( function(){

            if (jQuery("#try-now-loading").css("display") != "none") return;
            jQuery(this).hide();
            jQuery(".try-now-dialog").hide();
        });
    });
}

function showLoginDialog() {
    jQuery("#login-dialog").load("html/login_dialog.html", function() {
        showOverlay();
        showDialog("#login-dialog");

        jQuery("#dialog-overlay").click( function(){
            jQuery(this).hide();
            jQuery(".login-dialog").hide();
        });
    });
}

function showContactDialog() {
    jQuery("#contact-dialog").load("html/contact_dialog.html", function() {
        showOverlay();
        showDialog("#contact-dialog");

        jQuery("#dialog-overlay").click( function(){
            jQuery(this).hide();
            jQuery("#contact-dialog").hide();
        });
    });
}

function showMovie(newSource) {
    showOverlay();
    showDialog("#movie-dialog");

    var player = _V_("player", { "controls": true, "autoplay": true, "preload": "auto", techOrder:["html5","flash"], html5:{}, flash:{swf:"http://vjs.zencdn.net/c/video-js.swf"}, width:"auto", height:"auto"});
    player.src({ type: "video/mp4", src: newSource});

    jQuery("#dialog-overlay, #movie-dialog .close").click( function(){
        jQuery("#dialog-overlay").hide();
        jQuery("#movie-dialog").hide();
        player.pause();
    });
}

function hideLoginDialog() {
    jQuery("#dialog-overlay").hide();
    jQuery("#login-dialog").hide();
}

function hideTrialDialog() {
    jQuery("#dialog-overlay").hide();
    jQuery("#try-now-dialog").hide();
}

function hideContactDialog() {
    jQuery("#dialog-overlay").hide();
    jQuery("#contact-dialog").hide();
}


function clearText(field) {
    if (field.defaultValue == field.value) {
        field.value = "";
    }
}

function mask(field, type){
    if(type==1){
        $.mask.definitions['~']="[a-z]";
        jQuery("#try-now-business-name").mask("~~~?~~~~~~~~~~~~~~~~~~~~~~", {placeholder:" "});
    }
    if(type==2){
        jQuery("#try-now-phone-country").mask("?9999",{placeholder:" "});
    }
    if(type==3){
        jQuery("#try-now-phone").mask("?999999999999",{placeholder:" "});
    }
}

function resetText(field) {
    if (field.value == "") {
        field.value = field.defaultValue;
    }
}


function resetPasswordText(field) {
    if (field.value == "") {
        field.value = field.defaultValue;
        field.type = "text";
    }
}


function clearPasswordText(field) {
    if (field.defaultValue == field.value) {
        field.value = "";
        field.type = "password";
    }
}


function onContactFieldFocus(field) {
     if (field.defaultValue == field.value) {
        field.value = "";
        jQuery("#" + field.id).addClass("informed");
    }
}


function onContactFieldBlur(field) {
    if (field.value == "") {
        field.value = field.defaultValue;
        jQuery("#" + field.id).removeClass("informed");
    }
}


function validateNewAccountForm() {
    //Todos os inputs do form #try-now-form
    var formElements = jQuery("#try-now-form > input, select");
    var isValid = true;
    jQuery("#try-now-error-message").hide();
    jQuery("#try-now-error-message").html("");

    for (var i = 0; i < formElements.length; i++) {
        element = formElements[i];
        jQuery("#" + element.id).removeClass("invalid");
        if ((element.value == element.defaultValue || element.value == "") && element.type != "hidden") {
            jQuery("#" + element.id).addClass("invalid");
            isValid = false;
        }
    }

    if (isValid == false) {
        jQuery("#try-now-error-message").html("The highlighted fields are required.");
        jQuery("#try-now-error-message").show();
    }

    return isValid;
}


function createAccount() {
    if (validateNewAccountForm()) {
        hideCloseButtonCreateAccountLoading();
        toggleCreateAccountLoading();
        jQuery.ajax({
             type: 'post',
             url:'/asaas/register.php',
             dataType: "html",
             data: jQuery("#try-now-form").serialize(),
             success: function(data) {
		console.log(data);
                toggleCreateAccountLoading();
                var retorno = jQuery.parseJSON(data);
		console.log(retorno);
                if (retorno.status == "success") {
                    window.location = retorno.url;
                } else if (retorno.status == "error") {
		    showCloseButtonCreateAccountLoading();
                    showDialogErrorMessage(retorno.error);
                }
             },
             error: function(data) {
		showCloseButtonCreateAccountLoading();
//		var retorno = jQuery.parseJSON(data);
		console.log(data);
//		showDialogErrorMessage(retorno.error);
                showDialogErrorMessage("Ocorreu um erro ao processar sua requisição.");
                toggleCreateAccountLoading();
             }
        });
    }
}

function hideCloseButtonCreateAccountLoading(){
    jQuery("#try-now-dialog-close-button").hide();
}

function showCloseButtonCreateAccountLoading(){
    jQuery("#try-now-dialog-close-button").show();
}


function showDialogErrorMessage(message) {
    jQuery("#try-now-error-message").html(message);
    jQuery("#try-now-error-message").show();
}


function toggleCreateAccountLoading() {
    jQuery("#try-now-form").toggle();
    jQuery("#create-account-link").toggle();
    jQuery("#try-now-loading").toggle();
    jQuery("#login-link").toggle();
    jQuery("#login-form").toggle();
}


function scrollToRealISMS() {
    if (isHeaderVisible()) {
        hideHeader(402);
    } else {
        jQuery(window).scrollTop(402);
    }
}


function scrollToRealBCMS() {
    if (isHeaderVisible()) {
        hideHeader(1028);
    } else {
        jQuery(window).scrollTop(1028);
    }
}


function getSelectedProfile(option) {
    if (jQuery("#selected-option").val() != undefined) {
        option = jQuery("#selected-option").val();
    }
    return option;
}

function loadISMSPage(option) {
    switch(option){
        case IMPLEMENT:
            redirectTo("benefits_isms.html");
            break;
        case CONSULTANT:
            redirectTo("benefits_isms.html#consultant");
            break;
        case CERTIFIED:
            redirectTo("benefits_isms.html#certified");
            break;
        default:
            redirectTo("index_isms.html?option=" + getSelectedProfile(option));
    }
}

function loadBCMSPage(option) {
    switch(option){
        case IMPLEMENT:
            redirectTo("benefits_bcms.html");
            break;
        case CONSULTANT:
            redirectTo("benefits_bcms.html#consultant");
            break;
        case CERTIFIED:
            redirectTo("benefits_bcms.html#certified");
            break;
        default:
            redirectTo("index_bcms.html?option=" + getSelectedProfile(option));
    }
}

function redirectTo(url) {
    window.location = url;
}

function getBrowserWidth() {
    var width = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        width = window.innerWidth;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        width = document.documentElement.clientWidth;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        width = document.body.clientWidth;
    }

    return width;
}

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function getUserProducts(field) {
    if (field.value == "") {
        field.value = field.defaultValue;
        return;
    }

    jQuery.ajax({
         type: 'post',
         url:'/asaas/user_products.php',
         dataType: "html",
         data: "login=" + jQuery("#login").val(),//jQuery("#try-now-form").serialize(),
         success: function(data) {
            console.log(data);
            var retorno = jQuery.parseJSON(data);

            if (retorno.status == "success") {
                console.log(retorno.products);
                jQuery.each(retorno.products, function() {
                    jQuery("#login-product").append("<option value='" + this.product + "'>" + this.product + "</option>");
                    console.log(this);
                });
            } else if (retorno.status == "error") {
                console.log(retorno.error);
            }
         },
         error: function(data) {
            showDialogErrorMessage("An error has occurred while processing your request.");
            toggleCreateAccountLoading();
         }
    });
}

function login() {
    if (validateNewAccountForm()) {
        toggleCreateAccountLoading();
        jQuery.ajax({
             type: 'post',
             url:'/asaas/user_products.php',
             dataType: "html",
             data: jQuery("#login-form").serialize(),
             success: function(data) {
                toggleCreateAccountLoading();
                var retorno = jQuery.parseJSON(data);

                if (retorno.status == "success") {
                    window.location = retorno.url;
                } else if (retorno.status == "error") {
                    showDialogErrorMessage(retorno.error);
                }
             },
             error: function(data) {
                showDialogErrorMessage("An error has occurred while processing your request.");
                toggleCreateAccountLoading();
             }
        });
    }
}



function populateSelectInstances() {
    if(checkFields()==1){
        getInstances();
    }
}

function checkFields() {
    if(jQuery('#login-product').val()==""){
        showDialogErrorMessage("Select the product!");
        return 0;
    }
    if(jQuery('#login-email').val()==""){
        showDialogErrorMessage("Insert e-mail!");
        return 0;
    }
    return 1;
}

function getInstances(){

    var email = jQuery("#login-email").val();
    var product = jQuery("#login-product").val();
    var data = [
        {name: email},
        {name: product},
    ];

    jQuery.ajax({
        type: "POST",
        url: "/asaas/get_instances_user_product.php",
        data: {data: data},
        success: function(response){
            var retorno = jQuery.parseJSON(response);
            if (retorno.status == "success") {
                jQuery('#try-now-error-message').hide();
                jQuery('#login-button-gray').hide();
                jQuery('#login-button').show();
                selectInstanceList(retorno.inst);
            } else if (retorno.status == "error"){
                jQuery('#login-button-gray').show();
                jQuery('#login-button').hide();
                showDialogErrorMessage(retorno.error);
            }
        },
        error: function(response){
            showDialogErrorMessage("An error has occurred while processing your request.");
        }
    });
}


function cleanCombobox(){
    var el = document.getElementById('email-instances');
    if(el.hasChildNodes()){
        while( el.hasChildNodes() ){
            el.removeChild(el.lastChild);
        }
    }
}

function selectInstanceList(instances){
    cleanCombobox();
    if(instances.length > 1){
        addChooseInstanceText();
        for (var i=0; i< instances.length;i++){
            addMoreThenOneElement(instances[i]);
        }
    } else if(instances.length == 1) {
        addOneElement(instances[0]);
    }
}

function addChooseInstanceText(){
    jQuery('#email-instances').append('<option value="" id="choose-instance">Choose the instance</option>');
}

function addMoreThenOneElement(instance){
    jQuery('#choose-instance').after('<option value =' + instance + '>' + instance + '</option>');
}

function addOneElement(instance){
    jQuery('#email-instances').append('<option value =' + instance + '>' + instance + '</option>');
}

function sendEmail(param) {
    jQuery.ajax({
         type: 'post',
         url:'../enviar.php',
         dataType: "html",
         data: jQuery(param).serialize(),
         success: function(data) {
            var retorno = jQuery.parseJSON(data);
            if (retorno.success == "false")
                jQuery("#contact-message").html("Complete all fields!")
            else
                jQuery("#contact-message").html("Message successfully sent!")
         },
         error: function(data) {

         }
    });
}

function createAccount2() {

	if (validateNewAccountForm()) {
        hideCloseButtonCreateAccountLoading();
        jQuery.ajax({
             type: 'post',
             url:'../enviar-trial.php',
             dataType: "html",
             data: jQuery("#try-now-form").serialize(),
             success: function(data) {
				console.log(data);
                toggleCreateAccountLoading();
                var retorno = jQuery.parseJSON(data);
				console.log(retorno);
                if (retorno.success == "true") {
                    toggleCreateAccountLoading();
                    showCloseButtonCreateAccountLoading();
                } else if (retorno.success == "false") {
		    		showCloseButtonCreateAccountLoading();
                    showDialogErrorMessage("Complete all fields!");
                }
             },
             error: function(data) {
				showCloseButtonCreateAccountLoading();
				console.log(data);
                showDialogErrorMessage("Ocorreu um erro ao processar sua requisição.");
             }
        });
    }
}

var partnerIndex = 4;
function changePartners() {
    var partners = jQuery('#partners li');
    partners.hide('fast');

    for (var i=0; i<4; i++) {
        jQuery(partners[partnerIndex]).show('fast');
        partnerIndex++;
        if (partnerIndex >= partners.length) partnerIndex = 0;
    }
}
