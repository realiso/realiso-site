<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/modal.css" />
<link rel="stylesheet" type="text/css" href="css/menu.css" />

<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]--> 

<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="css/ie8.css" />
<![endif]--> 

<script src="js/jquery-1.8.3.min.js" language="javascript"></script>
<script src="js/slides.jquery.js" language="javascript"></script>
<script src="js/realiso.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"/></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js"/></script>
<script language="javascript">
	var curtain = null;
</script>
<!-- begin google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34780518-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- end google analytics -->
<!-- begin olark code -->

<!--script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('1107-497-10-4236');/*]]>*/</script><noscript><a href="https://www.olark.com/site/8431-136-10-6949/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1; IE=edge"/>
<title>RealISO - RealISMS - RealBCMS</title>
<meta name="robots" content="index,follow"/>
<meta name="Rating" content="General"/>
<meta name="Updated" content="Weekly"/>
<meta name="revisit-after" content="Weekly"/>
<meta name="Search Engines" CONTENT="AltaVista, AOLNet, Infoseek, Excite, Hotbot, Lycos, Magellan, LookSmart, CNET" />
<meta property="og:type" content="company"/>
<meta property="og:site_name" content="RealISO"/>
<meta name="description" content="RealISO ISMS, BCMS, SOX, ISO 27001 and ISO 22301 Application Suite" />
<meta name="keywords" content="realiso isms bcms iso27001 270001 iso22301 22301 sox sarbane-oxley saas" />
</head>

<body>
	<div>

		<div id="header">
		<? include('html/header/header.html'); ?>
		</div>

		<div id="header-bottom">
		<? include('html/header/bottom.html'); ?>
		</div>

		<div id="scrolling-area">

			<div id="slides-area">
			<? include('html/slides.html'); ?>
			</div>

			<div id="realisms-product-area">
			<? include('html/product/realisms.html'); ?>
			</div> 

			<div id="realbcms-product-area">
			<? include('html/product/realbcms.html'); ?>
			</div> 

			<div id="bottom">
			<? include('html/bottom.html'); ?>
			</div>

			<div id="footer">
			<? include('html/footer.html'); ?>
			</div>

		</div>
		
	</div>

	
	<!-- MODAL -->
	<div id="try-now-dialog" class="try-now-dialog">
	    
	</div>

	<!-- MODAL -->
	<div id="login-dialog" class="login-dialog">
	    
	</div>

 	<!-- mascara para cobrir o site -->  
	<div id="dialog-overlay"></div>

<script type="text/javascript">
	jQuery().ready(function(){
		includePages();

	});	
</script>	

</body>
</html>
