function includeISMSPages() {

    setTopISMSDimensions();


    jQuery("#header-bottom").load("html/header/product_bottom.html");
    jQuery("#header-bottom").css({"position": "fixed", "top" : "0px"});
    jQuery("#footer-isms").load("html/footer/footer_isms.html");
    jQuery("#footer-privacity").load("html/footer/footer_privacity.html");
}

function setTopISMSDimensions() {
    var i = 0;
    var width = jQuery(window).width() - 960;

    if (width % 2 != 0) {
        width = width - 1;
        i = 1;
    }

    jQuery(".product-top-left").css({"width" : (width/2) + "px"});
    jQuery(".product-top-right").css({"width" : (width/2) + "px"});
}