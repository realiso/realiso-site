<?
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
echo $lang;
switch ($lang){
    case "pt-br":
    case "pt":
        header("Location: /br");
        break;     
    case "en":
        header("Location: /en");
        break;        
    default:
        header("Location: /en");
        break;
}
?>
